import { readable } from 'svelte/store';

// export const time = readable(null, function start(set) {
//   // implementation goes here
//   const interval = setInterval(() => {
//     set(new Date());
//   }, 1000);
//
//   return function stop() {
//     clearInterval(interval);
//   };
// });

export const rcanton = readable(null, function start(set) {
  fetch('https://my.callofduty.com/api/papi-client/stats/cod/v1/title/mw/platform/psn/gamer/rcanton03/profile/type/mp')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      set(data.data);
    });
});

export const keeverrr = readable(null, function start(set) {
  fetch('https://my.callofduty.com/api/papi-client/stats/cod/v1/title/mw/platform/xbl/gamer/keeverrr/profile/type/mp')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
       set(data.data);
    });
});

export const speedy = readable(null, function start(set) {
  fetch('https://my.callofduty.com/api/papi-client/stats/cod/v1/title/mw/platform/xbl/gamer/speedyswing7974/profile/type/mp')
    .then((response) => {
      return response.json();
    })
    .then((data) => {
      set(data.data);
    });
});
